# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "voppe-jekyll-theme"
  spec.version       = "0.6.2"
  spec.authors       = ["voppe"]
  spec.email         = ["voppe@voppe.it"]

  spec.summary       = "The theme used for https://voppe.it"
  spec.homepage      = "https://voppe.it"
  spec.license       = "MIT"

  spec.files         = `ls`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|LICENSE|README)}i) }

  spec.add_development_dependency "jekyll", "~> 3.3"
  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
end
