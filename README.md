voppe-jekyll-theme
===

## Setup

Add this line to your Jekyll site's Gemfile:

```ruby
gem "voppe-jekyll-theme"
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: voppe-jekyll-theme
```

Execute installation:
```
    $ bundle install
```

## Usage

#### Menu

Create a file `menu.yml` in your `_data` folder. The file's structure is the following
``` yaml
items:
    *menuitems*
footer:
    *menuitems*
```

Where `menuitems` is an array of items with the following structure
```yaml
*section*:
    # If not specified defaults to '/*section*/'
    link: string 
    
    # Defaults to medium
    size: medium|large

    # Defines the "target" attribute of the link (e.g. "blank")
    target: string

    # The text to show on item hover
    label: string

    # A fontawesome icon name (e.g. 'envelope')
    icon: string

    # A background image link
    background: string 
```

Remember to update your `_config.yml` file to include your section
```yaml
collections: 
    *section*:
        output: true
```

#### List
Create a file in the site root named `*section*.md` and place this at the top of the file (front matter):
```
---
layout: list
collection: *collection*
---
```

To display a post's thumbnail, add a `thumbnail: *url*` to the post's front matter.
You can add a small description to the post using `description: *text*`.

## License

The theme is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).